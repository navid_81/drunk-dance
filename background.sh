# find others
declare -a arr=($(ps aux | grep -i "/mnt/sdb3/programming/scripts/background.sh" | grep -F '-' -v "--color"| awk '{print $2,$12}'))
declare -a pid=()
declare -a name=()

# path of images
declare image_path="/home/$USER/Pictures/backgrounds"

# background colors you want to use
declare -a red=(87 144 199 255 255)
declare -a green=(24 12 0 87 195)
declare -a blue=(69 62 57 51 0)

# find pid and name of other scripts running and kill other instances of this script
for (( index=0; index<${#arr[@]}; index+=2 )); do
    pid+=(${arr[index]})
done

for (( index=1; index<${#arr[@]}; index+=2 )); do
    name+=(${arr[index]})
done

for (( index=0; index<${#pid[@]}; index+=1 )); do
  if [ $BASHPID -ne ${pid[index]} ]; then
    printf 'killing [%q] \t %q \n' "${pid[index]}" "${name[index]}"
    kill ${pid[index]}
  fi
done

# change background color based on number of open terminals at the moment
declare count=$(ps aux | awk '{print $7}' | grep -v "?" | uniq | wc -l)
r=$(printf '%.2x\n' $((red[(count%5)])) )
g=$(printf '%.2x\n' $((green[(count%5)])) )
b=$(printf '%.2x\n' $((blue[(count%5)])) )
gsettings set org.gnome.desktop.background primary-color $(printf '#%q%q%q' "$r" "$g" "$b")

# now change images
for i in {1..4} # repeat image sequence 4 times
 do
  declare -a  arr=($(ls $image_path))  # read image filenames to display 
  for i in "${arr[@]}"
  do
    gsettings set org.gnome.desktop.background picture-uri file://$image_path/$i
    sleep 0.3
  done
done

