# what is it?
drun dance is a simple bash script which change your background when you open a terminal 
**but** it also set a series of of images during this process as your background, somehow
like a `creepy gif file`. 
background color change as well!


test it and you get what I say :)))

# warning
* for some reason, script does not run as I expected
* this script is still under development

# steps to setup

1- save images to `~/Pictures/backgrounds/` or anywhere you want (we call it `image_path`). you can use imaes I provided or any set of images you prefere.
size or format of images are not important. (use `tweaks>appearance>adjustment` if your image is too big/small)

2- save `background.sh` at any place you want (we call it `script_path`)

3- open `background.sh` and change `scipt_path` and `image_path` to corresponding values in step 1 and 2

4- add following command to the end of `~/.bashrc`. change `/mnt/sdb3/programming/scripts/background.sh` to your `scipt_path`

* `(/mnt/sdb3/programming/scripts/background.sh &) > /dev/null 2>&1`

5- start a new terminal

# note
* use `convert file.gif img.png` to convert a gif file to series of png images
* use `gimp` or any other image editor to make your images or simple use my cute anime dance bk
